// ==UserScript==
// @name         Google Test Script
// @version      0.4
// @description  try to take over the world!
// @author       You
// @match        https://www.google.com.ua
// @require      http://code.jquery.com/jquery-3.6.0.min.js
// @icon         https://www.google.com/s2/favicons?domain=google.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    document.addEventListener('keydown', onKeydown, true);
    function onKeydown(evt) {
        // Use https://keycode.info/ to get keys
        if (evt.altKey && evt.keyCode == 81) {
            onAltQ();
        }
    }
    function onAltQ() {
        $('input[name="q"]').val('New script');
        $('input[name="btnK"]').click();
    }
})();